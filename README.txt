This is a placeholder project for the i18n to Drupal 6-dev movement,
so we can accept patches against the code. Please read up on 
the project at http://groups.drupal.org/i18n if you are interested.

You can check out the code by anonymous SVN from this address:
http://svn3.cvsdude.com/devseed/sandbox/i18n/